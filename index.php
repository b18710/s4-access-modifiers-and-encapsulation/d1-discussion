<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S4</title>
</head>
<body>
	<h1>Access Modifiers</h1>

	<h3>Building</h3>
	<!-- <php $building->name = "Changed Name"; ?> -->
	<!-- <p><= $building->name; ?></p> -->

	<h3>Condominium</h3>
	<!-- <p><= $condominium->name; ?></p> -->

	<h3>Encapsulation</h3>
	<?php $condominium->setName('Enzo Tower'); ?>
	<?php echo $condominium->getName(); ?>

</body>
</html>